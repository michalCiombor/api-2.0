import React from 'react';
import './Passengers.css'





const Button = (props) => {
    return (
        <React.Fragment>
            <button onClick={props.click} disabled={props.disabled} id={props.name}>{props.text}</button>
        </React.Fragment>
    )
}


const AddToCart = (props) => {
    return (
        <React.Fragment>
            <Button name={props.name} click={props.clickSubstring} text="-" action="substract" disabled={props.state <= 0 ? true : false} />
            <span>{props.state}</span>
            <Button name={props.name} click={props.clickAdd} text="+" action="add" disabled={props.state >= 0 ? false : true} />

        </React.Fragment>
    )

}

class Passengers extends React.Component {


    render() {
        return (
            <>


                <AddToCart clickAdd={this.props.add} clickSubstring={this.props.substract} state={this.props.state} />

            </>
        )
    }
}



export default Passengers;