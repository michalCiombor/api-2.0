import React from "react";
import "./Inputs.css";

const Inputs = props => {
  return (
    <div>
      <div className="labels">
        <label htmlFor="from">From</label>
        <label htmlFor="to">To</label>
      </div>
      <div className="fromTo">
        <input
          value={props.value.origin}
          name="from"
          type="text"
          id="from"
          onChange={props.change}
          onBlur={()=>props.focus.bind(this, "origin")}
        />
        <input
          name="to"
          onChange={props.change}
          type="text"
          id="to"
          value={props.destination}
          onBlur={props.focus.bind(this, "departure")}
        />
      </div>
      <div className="labels">
        <label htmlFor="depart">Depart</label>
        <label htmlFor="return">Return</label>
      </div>
      <div className="dates">
        <input
          onChange={props.change}
          value={props.value.date}
          name="date"
          type="date"
          id="depart"
        />
        <input
          onChange={props.change}
          value={props.value.dateReturned}
          name="dateReturned"
          type="date"
          id="return"
          className={!props.isActive ? "" : "notActive"}
        />
      </div>
    </div>
  );
};

export default Inputs;
