import React from 'react';
import './SelectClass.css'

const SelectClass = (props) => {
    return (
        <div className="classChoice">
            <label htmlFor="select">Class</label>
            <select id="select" onChange={props.change} value={props.value}>


                <option value="E">Economy</option>
                <option value="P">Premium</option>
                <option value="PE">Premium Economy</option>
                <option value="B">Business</option>

            </select>
        </div>
    )
}

export default SelectClass;